System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function checkExecutionTime(isSeconds = false) {
        return function (target, propertyKey, descriptor) {
            const originalMethod = descriptor.value;
            descriptor.value = function (...args) {
                let unity = 'ms';
                let divisor = 1;
                if (isSeconds) {
                    unity = 's';
                    divisor = 1000;
                }
                console.log('----------------------');
                console.log(`Parâmetros passados para o método ${propertyKey}: ${JSON.stringify(args)}`);
                const t1 = performance.now();
                const retorno = originalMethod.apply(this, args);
                const t2 = performance.now();
                console.log(`O retorno do método ${propertyKey} é ${JSON.stringify(retorno)}`);
                console.log(`O método ${propertyKey} levou ${(t2 - t1) / divisor} ${unity} para ser executado`);
                return retorno;
            };
        };
    }
    exports_1("checkExecutionTime", checkExecutionTime);
    return {
        setters: [],
        execute: function () {
        }
    };
});
