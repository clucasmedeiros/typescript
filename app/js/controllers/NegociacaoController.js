System.register(["../views/index", "../models/index", "../helpers/decorators/index"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var index_1, index_2, index_3, NegociacaoController, weekDays;
    return {
        setters: [
            function (index_1_1) {
                index_1 = index_1_1;
            },
            function (index_2_1) {
                index_2 = index_2_1;
            },
            function (index_3_1) {
                index_3 = index_3_1;
            }
        ],
        execute: function () {
            NegociacaoController = class NegociacaoController {
                constructor() {
                    this._negociacoes = new index_2.Negociacoes();
                    this._negociacoesView = new index_1.NegociacoesView('#negociacoesView');
                    this._mensagemView = new index_1.MensagemView('#mensagemView', true);
                    this._negociacoesView.update(this._negociacoes);
                }
                add(event) {
                    event.preventDefault();
                    let date = new Date(this._inputData.val().replace(/-/g, ','));
                    if (!this._isBusinessDay(date)) {
                        this._mensagemView.update('Somente negociações em dias úteis, por favor.');
                        return;
                    }
                    const negociacao = new index_2.Negociacao(date, parseInt(this._inputQtd.val()), parseFloat(this._inputValor.val()));
                    this._negociacoes.add(negociacao);
                    this._negociacoesView.update(this._negociacoes);
                    this._mensagemView.update("Negociação adicionada com sucesso!");
                }
                _isBusinessDay(date) {
                    return date.getDay() != weekDays.Saturday && date.getDay() != weekDays.Sunday;
                }
                importData() {
                    function isOk(res) {
                        if (res.ok) {
                            return res;
                        }
                        else {
                            throw new Error(res.statusText);
                        }
                    }
                    fetch('http://localhost:8080/dados')
                        .then(res => isOk(res))
                        .then(res => res.json())
                        .then((data) => {
                        data
                            .map(dado => new index_2.Negociacao(new Date(), dado.vezes, dado.montante))
                            .forEach(negociation => this._negociacoes.add(negociation));
                        this._negociacoesView.update(this._negociacoes);
                    })
                        .catch(err => console.log(err.message));
                }
            };
            __decorate([
                index_3.domInject('#data')
            ], NegociacaoController.prototype, "_inputData", void 0);
            __decorate([
                index_3.domInject('#quantidade')
            ], NegociacaoController.prototype, "_inputQtd", void 0);
            __decorate([
                index_3.domInject('#valor')
            ], NegociacaoController.prototype, "_inputValor", void 0);
            exports_1("NegociacaoController", NegociacaoController);
            (function (weekDays) {
                weekDays[weekDays["Sunday"] = 0] = "Sunday";
                weekDays[weekDays["Monday"] = 1] = "Monday";
                weekDays[weekDays["Thursday"] = 2] = "Thursday";
                weekDays[weekDays["Wednesday"] = 3] = "Wednesday";
                weekDays[weekDays["Tuesday"] = 4] = "Tuesday";
                weekDays[weekDays["Friday"] = 5] = "Friday";
                weekDays[weekDays["Saturday"] = 6] = "Saturday";
            })(weekDays || (weekDays = {}));
        }
    };
});
