export function checkExecutionTime(isSeconds: boolean = false) {

    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {

        const originalMethod = descriptor.value;

        descriptor.value = function(...args: any[]) {
            let unity = 'ms';
            let divisor = 1;
            if(isSeconds) {
                unity = 's';
                divisor = 1000;
            }
             
            console.log('----------------------');
            console.log(`Parâmetros passados para o método ${propertyKey}: ${JSON.stringify(args)}`);

            const t1 = performance.now();
            
            const retorno = originalMethod.apply(this, args);

            const t2 = performance.now();

            console.log(`O retorno do método ${propertyKey} é ${JSON.stringify(retorno)}`);

            console.log(`O método ${propertyKey} levou ${(t2-t1)/divisor} ${unity} para ser executado`);

            return retorno;

        }
        
    }

}