import { NegociacoesView, MensagemView } from '../views/index';
import { Negociacoes, Negociacao } from '../models/index';
import { checkExecutionTime, domInject } from '../helpers/decorators/index';

export class NegociacaoController {

    @domInject('#data')
    private _inputData: JQuery;

    @domInject('#quantidade')
    private _inputQtd: JQuery;

    @domInject('#valor')
    private _inputValor: JQuery;
    private _negociacoes = new Negociacoes();
    private _negociacoesView = new NegociacoesView('#negociacoesView');
    private _mensagemView = new MensagemView('#mensagemView', true);

    constructor() {
        this._negociacoesView.update(this._negociacoes);
    }

    add(event: Event) {

        event.preventDefault();

        let date = new Date(this._inputData.val().replace(/-/g, ','));

        if(!this._isBusinessDay(date)) {
            this._mensagemView.update('Somente negociações em dias úteis, por favor.');
            return;
        }

        const negociacao = new Negociacao(
            date,
            parseInt(this._inputQtd.val()),
            parseFloat(this._inputValor.val()),
        );

        this._negociacoes.add(negociacao);

        this._negociacoesView.update(this._negociacoes);
        this._mensagemView.update("Negociação adicionada com sucesso!");
    }

    private _isBusinessDay(date: Date) {
        return date.getDay() != weekDays.Saturday && date.getDay() != weekDays.Sunday;
    }

    importData() {

        function isOk(res: Response) {
            if(res.ok) {
                return res;
            } else {
                throw new Error(res.statusText);
            }
        }

        fetch('http://localhost:8080/dados')
            .then(res => isOk(res))
            .then(res => res.json())
            .then((data: any[]) => {
                data
                    .map(dado => new Negociacao(new Date(), dado.vezes, dado.montante))
                    .forEach(negociation => this._negociacoes.add(negociation))
                this._negociacoesView.update(this._negociacoes);
            })
            .catch(err => console.log(err.message));
            
    }

}

enum weekDays {

    Sunday,
    Monday,
    Thursday,
    Wednesday,
    Tuesday,
    Friday,
    Saturday,

}