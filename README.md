# Typescript

Descrição

## Getting Started

Instruções

### Prerequisites

Nodejs
Typescript


### Installing

Nodejs
sudo apt-get install -y nodejs

```
ATENÇÃO: em algumas distribuições Linux, pode haver um conflito de nomes quando o 
Node é instalado pelo apt-get. Neste caso específico, no lugar do binário ser node, 
ele passa a se chamar nodejs. Isso gera problemas, pois a instrução npm start não 
funcionará, pois ela procura o binário node e não nodejs. Para resolver no Ubuntu
```

Typescript

npm i typescript
